import observe from "./observe.js";
const button = document.getElementsByTagName("button");
fn1.addEventListener("click", function () {
  data.a.b++;
});
fn2.addEventListener("click", function () {
  data.c[3] = 99; // 通过数组下标修改数组无法被响应式系统侦测到
  //   data.c[3].f = 7;
  console.log(data.c[0]);
});
import Watcher from "./Watcher.js";
let data = {
  a: {
    b: 99,
  },
  c: [1, 2, 3, { f: 6 }],
};

observe(data);

// 这里实现的Watcher与Vue2中基本的watch一样，只能侦听到一层数据
new Watcher(data, "a", (newVal, oldVal) => {
  console.log("a新的值--》", newVal);
  console.log("a老的值--》", oldVal);
});

new Watcher(data, "a.b", (newVal, oldVal) => {
  console.log("a.b新的值--》", newVal);
  console.log("a.b老的值--》", oldVal);
});
new Watcher(data, "c", (newVal, oldVal) => {
  console.log("c新的值--》", newVal);
  console.log("c老的值--》", oldVal);
});
