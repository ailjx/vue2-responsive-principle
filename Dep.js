export default class Dep {
    constructor() {
            //存储自己的订阅者，即Watcher实例
            this.subs = [];
        }
        //添加订阅
    addSub(sub) {
            this.subs.push(sub);
        }
        //添加依赖
    depend() {
            //window.target是我们指定的全局唯一的位置，当有依赖产生时Watcher会将依赖绑定到该位置
            if (window.target) {
                this.addSub(window.target);
            }
        }
        //通知更新
    notify() {
        //浅克隆:防止循环引用
        const subs = this.subs.slice();
        for (let i = 0, l = subs.length; i < l; i++) {
            subs[i].update();
        }
    }
}