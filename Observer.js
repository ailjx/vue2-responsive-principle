/*
 * @Author: AiLjx
 * @Date: 2023-03-02 20:19:46
 * @LastEditors: AiLjx
 * @LastEditTime: 2023-03-02 21:59:12
 */
import { def } from "./utils.js";
import defineReactive from "./defineReactive.js";
import { arrayMethods } from "./array.js";
import observe from "./observe.js";
import Dep from "./Dep.js";
export default class Observer {
  constructor(value) {
    //每个Observer的实例上都有一个Dep
    this.dep = new Dep();
    // console.log("Observer创建了dep在", value, "的", this, "上");
    //构造函数中的this不是表示类本身，而是表示实例
    //给实例添加了__ob__属性，值是这次new的实例
    def(value, "__ob__", this, false);
    //Observer类的目的是：将一个正常的object转换为
    //每个层级的属性都是响应式的object
    //检查它是不是数组
    if (Array.isArray(value)) {
      //如果是数组，将该数组的原型指向我们定义的arrayMethods
      //以便使用数组那7个方法时是调用arrayMethods上我们处理过的而不是Array.prototype上的
      Object.setPrototypeOf(value, arrayMethods);
      this.observeArray(value);
    } else {
      this.walk(value);
    }
  }
  //遍历对象
  walk(value) {
    for (let k in value) {
      defineReactive(value, k);
    }
  }
  //遍历数组
  observeArray(arg) {
    //这里不直接i < l.length，是为了防止数组在遍历过程中出现长度的变化
    for (let i = 0, l = arg.length; i < l; i++) {
      // 逐项进行observe
      // 如果arg[i]是基本处理类型，则在observe中会直接return
      observe(arg[i]);
    }
  }
}
